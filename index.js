const app = require('express')()

const host = '127.0.0.1'
const port = 7000

app.get('/', (req, res) => {
  res.status(200).type('text/plain');
  res.send("Hello, Forward");
});

app.get('/store', (req, res) => {
  res.status(200).type('text/plain');
  let data = req.query.data;
  client.set("data", data);
  res.send("Ok, stored value from data param is: " + data);
});

app.get('/get', (req, res) => {
  res.status(200).type('text/plain');
  client.get('data', (err, value) => {
    res.send('Last stored data: ' + value);
  });
});

app.get('/notify', (req, res) => {
  res.status(200).type('text/plain');
  client.get('data', (err, value) => {
    bot.telegram.sendMessage(chatId, "Stored value from data param is: " + value);
    res.send('Sent: ' + value);
  });
});

app.use((req, res, next) => {
  res.status(404).type('text/plain')
  res.send('Not found')
});

app.listen(port, host, function() {
  console.log(`Server listens http://${host}:${port}`)
});

// redis
const redis = require("redis");
const client = redis.createClient();

client.on("error", function(error) {
  console.error(error);
});

client.set("key", "value", redis.print);
client.get("key", redis.print);

// telegraph
const Telegraf = require('telegraf');
const fs = require('fs');
const chatId = XXXXXXXXX; // указать chatId
const bot = new Telegraf("123456789:AbCdfGhIJKlmNoQQRsTUVwxyZ"); // указать BOT_TOKEN